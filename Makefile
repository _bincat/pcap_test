all: pcap_test

pcap_test: pcap_test.o 
	gcc -o pcap_test pcap_test.o -lpcap

pcap.o: pcap_test.c
	gcc -c pcap_test.c 

clean:
	rm *.o pcap_test

