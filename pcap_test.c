#include <pcap.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <linux/icmp.h>
#include <netinet/tcp.h>


#define DUMP_OFFSET 16
#define MAC_DST_OFFSET 0
#define MAC_SRC_OFFSET 6
#define IS_DST 0
#define IS_SRC 1
#define ETHERTYPE_OFFSET 12
#define IP_HEADER_OFFSET 14
#define IP_SRC_OFFSET 12
#define IP_DST_OFFSET 16
#define PRTC_ID_OFFSET 9
#define TCP_OFFSET 20
#define TCP_DATA_OFFSET 54
#define TCP_SRC_PORT 0
#define TCP_DST_PORT 2

uint16_t filter_port;
unsigned int filter_check = 0;

void hexdump (const u_char *packet, bpf_u_int32 len)
{
  unsigned i = 0;

  while (i < len)
  {
    //const char* dump_format = "%8d: %x%x %x%x %x%x %x%x\t%8s\n"
    printf("%08x: ", i);

    unsigned j = 0;
    while (j < DUMP_OFFSET)
    {
      if ((i + j < len))
        printf ("%02x", 0xff & (char) packet[i+j]);
      else
        printf ("  ");

      if ((i + j + 1< len))
        printf ("%02x ", 0xff & (char) packet[i+j+1]);
      else
        printf ("   ");
      j += 2;
    }

    j = 0;
    while (j < DUMP_OFFSET)
    {
      char c = (char) packet[i+j];


      if (c >= 0x20 && c <0x7f)
        putchar (c);

      else
        putchar ('.');

      j++;

      if (! (i + j < len))
        break;
    }
    puts ("");
    i += 0x10;
  }
}


uint16_t parse_tcp_packet (struct tcphdr* packet)
{
  uint16_t src_port;
  uint16_t dst_port;
  struct tcphdr* tcp_hdr = packet;

  src_port = ntohs (tcp_hdr->source);
  dst_port = ntohs (tcp_hdr->dest);

  printf ("TCP_SRC_PORT = %hu\n", src_port);
  printf ("TCP_DST_PORT = %hu\n", dst_port);

  if (src_port == filter_port || dst_port == filter_port)
  {
    filter_check = 1;
  }
  return tcp_hdr->doff*4;
}

uint32_t parse_ip_packet (struct ip* packet)
{
  char ip_dst[20] = {0};
  char ip_src[20] = {0};
  uint8_t prtc_id;
  struct ip* ip_hdr;

  ip_hdr = packet;
  inet_ntop (AF_INET, &ip_hdr->ip_src, ip_src, sizeof (ip_src));
  inet_ntop (AF_INET, &ip_hdr->ip_dst, ip_dst, sizeof (ip_dst));
  printf ("IP SRC = %s\n", ip_src);
  printf ("IP DST = %s\n", ip_dst);
  prtc_id = ip_hdr->ip_p;
  printf ("PROTOCOL = 0x%x\n", prtc_id);

  if ((int)prtc_id != IPPROTO_TCP) // 6
  {
    puts ("Not Implemented");
    return 0;
  }
  //printf ("%hu\n", ip_hdr->ip_len);

  return (uint32_t) parse_tcp_packet ((struct tcphdr*)((char *)packet + ip_hdr->ip_hl*4)) + ip_hdr->ip_hl*4;
}

uint32_t parse_eth_packet (struct ethhdr* packet)
{
  uint16_t eth_type;
  struct ethhdr* ether_ptr;

  ether_ptr = packet;

  printf ("DST MAC = %s\n", ether_ntoa ((struct ether_addr*) ether_ptr->h_dest));
  printf ("SRC MAC = %s\n", ether_ntoa ((struct ether_addr*) ether_ptr->h_source));
  eth_type = ntohs (ether_ptr->h_proto);
  printf ("ETH TYP = 0x%04x\n", ntohs (ether_ptr->h_proto));

  if (eth_type != ETHERTYPE_IP) // 0x0800
  {
    puts ("Not Implemented");
    return 0;
  }

  return parse_ip_packet ((struct ip *)((char *) packet + sizeof (struct ethhdr)));
}

int main(int argc, char *argv[])
{
  pcap_t *handle;     /* Session handle */
  char *dev;      /* The device to sniff on */
  char errbuf[PCAP_ERRBUF_SIZE];  /* Error string */
  struct bpf_program fp;    /* The compiled filter */
  //char filter_exp[] = "port 80";// or port 443";  /* The filter expression */
  bpf_u_int32 mask;   /* Our netmask */
  bpf_u_int32 net;    /* Our IP */
  struct pcap_pkthdr *header;  /* The header that pcap gives us */
  const u_char *packet;   /* The actual packet */
  char* filter_exp;

  //default : port 80
  filter_exp = "80";
 
  if (argv[2] && strlen (argv[2]) > 0)
    filter_exp = argv[2];

  filter_port = (uint16_t)(atoi (filter_exp));
  
  /* Define the device */
  /*
  dev = pcap_lookupdev(errbuf);
  if (dev == NULL) {
    fprintf(stderr, "Couldn't find default device: %s\n", errbuf);
    return(2);
  }
   Find the properties for the device 
  if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
    fprintf(stderr, "Couldn't get netmask for device %s: %s\n", dev, errbuf);
    net = 0;
    mask = 0;
  }*/
  /* Open the session in promiscuous mode */
  handle = pcap_open_live(argv[1], BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
    return(2);
  }
  /* Compile and apply the filter */
  /*if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
    fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(handle));
    return(2);
  }
  if (pcap_setfilter(handle, &fp) == -1) {
    fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
    return(2);
  }*/
  while (1)
  {
    /* Grab a packet */
    int res;
    res = pcap_next_ex(handle, &header, &packet);
    /* Print its length */
    //printf("res -> %d\n", res);
    // printf("Jacked a packet with length of [%d]\n", header->caplen);

    if (res > 0)
    {
      uint32_t dump_len;
      uint32_t offset;
      filter_check = 0;
      puts ("============ PACKET INFO ===========");

      offset = 14 + parse_eth_packet ((struct ethhdr*) packet);

      //printf ("dump_len = %u\n", header->caplen);

      if ((offset > 14) && (filter_check == 1))
      {
        dump_len = header->caplen - offset;
        hexdump (packet + offset, dump_len);
      }
      filter_check = 0;
      puts ("\n\n\n\n");
    }
  }
  /* And close the session */
  pcap_close(handle);
  return 0;
}
